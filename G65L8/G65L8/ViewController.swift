//
//  ViewController.swift
//  G65L8
//
//  Created by Ivan Vasilevich on 9/6/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

struct Student {
	var name: String
	var image: UIImage
}

class StudentClass {
	var name: String = ""
	var image: UIImage = UIImage()
}

class ViewController: UIViewController, UITableViewDataSource {

	@IBOutlet weak var tableView: UITableView!
	var studentsNames = "Andrey, Alexandr, Grisha, Stas, Ivan".components(separatedBy: ", ")
	var students = [Student]()
	
	
	// MARK: - VC Lificycle
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		view.backgroundColor = UIColor.init(rgb: 0x9e34a8)
		fillStudents()
		tableView.dataSource = self
		tableView.delegate = self
	}
	
	func fillStudents() {
		for name in studentsNames {
			let newStudent = Student(name: name, image: UIImage.init(named: name)!)
			students.append(newStudent)
		}
	}
	
	// MARK: - UITableViewDataSource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return students.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		let studentToDisplay = students[indexPath.row]
		cell.textLabel?.text = studentToDisplay.name
		cell.imageView?.image = studentToDisplay.image
		return cell
	}
}

extension ViewController: UITableViewDelegate {
	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print(indexPath)
	}
	
	func foo() {
		var newStudent = Student(name: "Ivan", image: UIImage.init(named: "Ivan")!)
		print(newStudent.name)
		newStudent.name = "fdsf"
		var newSt2 = newStudent
		newSt2.name = "Vasiliy"
		bar(student: newStudent)
		print(newStudent.name)
		
	}
	
	func foo2() {
		var newStudent = StudentClass()
		newStudent.name = "Ivan2"
		newStudent.image = UIImage.init(named: "Ivan")!
		print(newStudent.name)
		newStudent.name = "fdsf"
		var newSt2 = newStudent
		newSt2.name = "Vasiliy"
		print(newStudent.name)
		
	}
	
	func bar(student: Student) {
//		student.name = "Azazas"
	}
}

