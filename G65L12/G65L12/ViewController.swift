//
//  ViewController.swift
//  G65L12
//
//  Created by Ivan Vasilevich on 9/20/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import MessageUI

class ViewController: UIViewController, MFMessageComposeViewControllerDelegate {
	
	var player: AVAudioPlayer!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let imageView = UIImageView.init(frame: view.frame)
		imageView.backgroundColor = .yellow
		imageView.sd_setImage(with: "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2018/09/18234008/rsmeeting2018_dalle_carbonare_156.jpg".url, completed: nil)
		imageView.contentMode = .scaleAspectFit
		view.addSubview(imageView)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let fileUrl = Bundle.main.url(forResource: "bgMusic", withExtension: "mp3")
		player = try! AVAudioPlayer.init(contentsOf: fileUrl!)
		player.play()
		
		let composeVC = MFMessageComposeViewController()
		composeVC.messageComposeDelegate = self
		
		// Configure the fields of the interface.
		composeVC.recipients = ["4085551212"]
		composeVC.body = "Hello from California!"
		
		// Present the view controller modally.
		self.present(composeVC, animated: true, completion: nil)
	}
	
	func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
		controller.dismiss(animated: true, completion: nil)
	}
	
}

extension String {
	var url: URL? {
		return URL.init(string: self)
	}
}

