//
//  ViewController.swift
//  G65L5
//
//  Created by Ivan Vasilevich on 8/28/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let a = foo()
		print("a = \(a)")
		
		let humanBoy = Human.init(gender: .boy)
		humanBoy.weight = 50_000
		humanBoy.age = 8
		humanBoy.height = 1_200
		print("nash malchik is: \(humanBoy.description)")
		humanBoy.sleep()
		humanBoy.eat(foodWeight: 3000)
		print("nash malchik is: \(humanBoy.description)")
		drawBox()
	}
	
	func foo() -> Int {
		return 6
	}

	func drawBox() {
		let rect = CGRect.init(x: 20, y: 40, width: 32, height: 32)
		let box = UIView.init(frame: rect)
//		box.frame.size.width = 35
		box.backgroundColor = .red
		view.addSubview(box)
		
		
	}
}

